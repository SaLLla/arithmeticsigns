﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _5_ArithmeticSigns
{
    class Program
    {
        static void Main(string[] args)
        {
            DateProcessing a = new DateProcessing();
            
            a.Center();
            Console.ReadLine();
        }
    }

    class InputData
    {
        public double Rezult { get; private set; }

        public List<double> Numbers { get; private set; }

        public InputData()
        {
            Numbers = new List<double>();
            foreach (var x in InputNumbers())
            {
                Numbers.Add(x);
            }

            Rezult = InputRezult();
        }

        List<double> InputNumbers()
        {
            List<double> number = new List<double>();
            Console.WriteLine("Введите через пробел числа, над которыми\nнужно провести арифметические операции.");
            string[] numbers = Console.ReadLine().Split(new char[] { ' ' }, 11, StringSplitOptions.RemoveEmptyEntries);

            foreach (var x in numbers)
            {
                try
                {
                    number.Add(Convert.ToInt32(x));
                }
                catch (FormatException)
                {
                    Console.WriteLine("Введены недопустимые символы.");
                    Console.ReadLine();

                    return null;
                }
                if (number.Count == 10)
                {
                    Console.WriteLine("Интересный факт: в этой программе можно ввести максимум 10 чисел :)");
                    return number;
                }
            }
            return number;
        }//переработать

        double InputRezult()
        {
            Console.WriteLine("Введите результат арифметических операций.");
            bool _Error = false;
            do{
                try
                {
                    return Convert.ToDouble(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Введены недопустимые символы. Повторите попытку.");
                    _Error = true;
                }
            }while(_Error);
            return 0;
        }
    }

    class Signs : InputData
    {
        public List<int> x = new List<int>();

        public List<int> CodeOfSigns/////////////////////////////////////////////////
        {
            get
            {
                return x;
            }
            set
            {
                x = CenterOfSigns(value[0]);
            }
        }

        protected List<int> CenterOfSigns(int _CodeSigns)
        {
            _CodeSigns = ConverterTo4NumberSystem(ref _CodeSigns);
            return BreakdownCodeSigns(_CodeSigns);
        }

        int ConverterTo4NumberSystem(ref int number)
        {
            int j = 0;
            double k = 0;
            while (number != 0)
            {
                int i = number % 4;
                number /= 4;

                j += Convert.ToInt32(Math.Pow(10, k) * i);
                k++;
            }
            return j;
        }

        List<int> BreakdownCodeSigns(int _CodeSigns)
        {
            List<int> NumbersOfSigns = new List<int>();
            for (int i = 0; i < Numbers.Count - 1; i++)//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            {
                NumbersOfSigns.Add(_CodeSigns % 10);
                _CodeSigns /= 10;
            }
            return NumbersOfSigns;
        }
    }

    class DateProcessing : Signs
    {

        List<double> CopyNumbers = new List<double>();
        List<int> CopyCodeOfSigns = new List<int>();

        public DateProcessing()
        {
            CopyList1ToList2(Numbers, CopyNumbers);
        }

        List<int> CopyList1ToList2(List<int> list1, List<int> list2)
        {
            list2.Clear();
            foreach (var x in list1)
            {
                list2.Add(x);
            }
            return list2;
        }

        List<double> CopyList1ToList2(List<double> list1, List<double> list2)
        {
            list2.Clear();
            foreach (var x in list1)
            {
                list2.Add(x);
            }
            return list2;
        }

        public void Center()
        {
            bool _flag = true;

            for (int _CodeOfSigns = 0; _CodeOfSigns < Math.Pow(4, Numbers.Count - 1); _CodeOfSigns++)
            {
                List<int> a = new List<int>();
                a.Clear();
                a.Add(_CodeOfSigns);
                CopyList1ToList2(Numbers, CopyNumbers);
                CodeOfSigns = a;
                CopyList1ToList2(CodeOfSigns, CopyCodeOfSigns);

                CheckMinus(ref CopyNumbers, CopyCodeOfSigns);
                try
                {
                    MultiplicationAndDivision(ref CopyNumbers, ref CopyCodeOfSigns);
                }
                catch (DivideByZeroException)
                {
                    continue;
                }

                if (CopyNumbers.Sum() == Rezult)
                {
                    OutputRezult();
                    _flag = false;
                }
            }
            if (_flag)
            {
                Console.WriteLine("Результатов нет.");
            }
        }

        void MultiplicationAndDivision(ref List<double> Numbers, ref List<int> Signs)
        {
            
            //foreach (int x in Signs)
            for (int _index = 0; _index < Signs.Count; _index++)
            {
                switch (Signs[_index])
                {
                    case 2:
                        {
                            Numbers[_index] *= Numbers[_index + 1];
                            Numbers.RemoveAt(_index + 1);
                            Signs.RemoveAt(_index);
                            _index--;
                            break;
                        }
                    case 3:
                        {
                            Numbers[_index] /= Numbers[_index + 1];
                            Numbers.RemoveAt(_index + 1);
                            Signs.RemoveAt(_index);
                            _index--;
                            break;
                        }
                }
            }
            return;
        }

        List<double> CheckMinus(ref List<double> Numbers, List<int> Signs)
        {
            int _index = 0;
            foreach (int x in Signs)
            {
                if (x == 1)
                {
                    Numbers[_index + 1] *= -1;
                }
                _index++;
            }
            return Numbers;
        }
        
        void OutputRezult()
        {
            int _index = 0;
            
            foreach (int x in Numbers)
            {
                try
                {
                    Console.Write(x + " " + Sign(CodeOfSigns[_index]) + " ");
                    _index++;
                }
              
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine(Numbers.Last() + " = " + Rezult);
                    return;
                }
            }
        }

        string Sign(int _number)
        {
            switch (_number)
            {
                case 0: 
                    return "+"; 
                case 1:
                    return "-";
                case 2:
                    return "*";
                case 3:
                    return "/";
            }
            return null;
        }
    }
}
